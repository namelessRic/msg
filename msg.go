package msg

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/errors"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	sms "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/sms/v20210111" // 引入sms
)

type Cfg struct {
	SignName   string
	TemplateId string
	FtKey      string
	Appid      string
	Secret     string
}

func NewMsgClient(sign, template, ft, appid, secret string) *Cfg {
	return &Cfg{
		SignName:   sign,
		TemplateId: template,
		FtKey:      ft,
		Appid:      appid,
		Secret:     secret,
	}
}

func (cfg *Cfg) Send(phoneNum string, params []string) (string, error) {
	credential := common.NewCredential(
		cfg.Appid,
		cfg.Secret,
	)
	cpf := profile.NewClientProfile()
	cpf.HttpProfile.ReqMethod = "POST"
	cpf.HttpProfile.Endpoint = "sms.tencentcloudapi.com"
	cpf.SignMethod = "HmacSHA1"
	client, _ := sms.NewClient(credential, "ap-guangzhou", cpf)
	request := sms.NewSendSmsRequest()
	request.SmsSdkAppId = common.StringPtr("1400568563")
	request.SignName = common.StringPtr(cfg.SignName)
	request.SenderId = common.StringPtr("")
	request.SessionContext = common.StringPtr("xxx")
	request.ExtendCode = common.StringPtr("")

	/* 模板参数: 若无模板参数，则设置为空*/
	request.TemplateParamSet = common.StringPtrs(params)
	/* 模板 ID: 必须填写已审核通过的模板 ID。模板ID可登录 [短信控制台] 查看 */
	request.TemplateId = common.StringPtr(cfg.TemplateId)
	request.PhoneNumberSet = common.StringPtrs([]string{"+86" + phoneNum})

	response, err := client.SendSms(request)
	// 处理异常
	if _, ok := err.(*errors.TencentCloudSDKError); ok {
		fmt.Printf("An API error has returned: %s", err)
		return fmt.Sprintf("An API error has returned: %s", err), err
	}
	// 非SDK异常，直接失败。实际代码中可以加入其他的处理。
	if err != nil {
		return "", err
	}
	if *response.Response.SendStatusSet[0].Fee != 1 {
		return "", fmt.Errorf(*response.Response.SendStatusSet[0].Message)
	}
	b, _ := json.Marshal(response.Response)
	// 打印返回的json字符串
	return string(b), nil
}

func (cfg *Cfg) SendFt(msg string) (string, error) {
	client := &http.Client{}
	reqest, err := http.NewRequest("GET", "https://sc.ftqq.com/"+cfg.FtKey+".send?text="+msg, nil) //建立一个请求
	if err != nil {
		return "", err
	}
	response, err := client.Do(reqest)
	if err != nil {
		return "", err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}
